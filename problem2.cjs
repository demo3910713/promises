// import the fs modules
const fs = require("fs").promises;

// read the files
function readFile(name) {
  try{
      return fs.readFile(name, "utf-8");
  }catch(err){
    console.error(err)
  }
}
// create the files
function createFile(filename, data) {
  try{
       return fs.writeFile(filename, data);
  }catch(err){
    console.error(err)
  }
}
// save the files in filesname.txt
function saveName(filename) {
  try{
    return fs.appendFile("filesname.txt", filename);
  }catch(err){
    console.error(err)
  }
}
// delete the files
function deleteFile(filename) {
  try{
    fs.unlink(filename);
  }catch(err){
    console.error(err)
  }
  
}
// exports the all functions
module.exports = { readFile, createFile, saveName, deleteFile };
