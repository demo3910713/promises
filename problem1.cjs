//import the fs modules
const fs = require("fs").promises;
// creating directory
function makeDirectory(floderName){
 try{
     return  fs.mkdir(floderName)
 }catch(err){
    console.error(err)
 }
}
// creating random json files
function createFiles(filename, data){
    try{
     return fs.writeFile(filename, data)
 }catch(err){
    console.error(err)
 }
  
}
// delete files
function deleteFiles(filename){
   try{
      return fs.unlink(filename)
   }catch(err){
      console.error(err)
   }
}

// export the functions 
module.exports = {makeDirectory, createFiles, deleteFiles}
