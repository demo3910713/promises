// import the functions from the problem1.cjs
const {createFiles, deleteFiles, makeDirectory}= require('../problem1.cjs')
// Directory name 
const directoryName = "floder"
// no of files 
const noOfFiles =  5
function main(){
try {
    // making directory
    makeDirectory(directoryName).then(()=>{
        console.log('Directory Created')
        // store the filecreated promises
        let fileCreatedPromises = []
        // store the filedeleted promises 
        let deletedPromises = []
        for (let index =1; index <= noOfFiles; index++) {
            let fileName =`./${directoryName}/random${index}.json`
            let data = `${Math.random()}`
            // file creation
        fileCreatedPromises.push (createFiles(fileName, data))
        console.log(`created ${fileName}`)
        // file deletion
        deletedPromises.push( deleteFiles(fileName))
        console.log(`deleted  ${fileName}`)
        }       
        return Promise.all([...fileCreatedPromises, ...deletedPromises])
    })
    .then(()=>{
        console.log('successfully completed..!')
    })
    .catch((err)=>{
        console.error(err);
    })
    
} catch (error) {
    console.error(error);
    
}
}

main()