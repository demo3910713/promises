// import the problem2.cjs
const{createFile,readFile, deleteFile, saveName} = require("../problem2.cjs");

// file name
const filename = "./lipsum_1.txt";

function main(){
try {
  // read the file lipsum_1.txt
    readFile(filename)
    .then((data) => {
      // convert the data into the uppercase
      let value = data.toUpperCase();
      // store the converting data into new file
      let createFilePromise = createFile("toUpperCase.txt", value);
      // store the filename into the filesname.txt
      let saveNamePromise = saveName("toUpperCase.txt ");
      return Promise.all([createFilePromise, saveNamePromise]);
    })
    .then((data) => {
      console.log("successfully created  uppercase.txt");
      // read the new file
      return readFile("toUpperCase.txt");
    })
    .then((data) => {
      // convert the data into lowercase and split into the sentences
      let sentences = JSON.stringify(data.toLocaleLowerCase().split(". "));
      // store the converting data into new file
      let createFilePromise = createFile(
        "toLowerCase.txt",
        sentences
      );
      // store the filename into the filesname.txt
      let saveNamePromise = saveName("toLowerCase.txt ");
      return Promise.all([createFilePromise, saveNamePromise]);
    })
    .then((data) => {
      console.log("successfully created lowercase.txt");
      return readFile("toLowerCase.txt");
    })
    .then((data) => {
      // sorting the data
      let sorting = JSON.parse(data).sort();
      // store the sorted data into new file
      let createFilePromise = createFile("sorting.txt", sorting);
      // save the filename into the filesname.txt
      let saveNamePromise = saveName("sorting.txt ");
      return Promise.all([createFilePromise, saveNamePromise]);
    })
    .then(() => {
      console.log("successfully created sorting.txt");
      // read the filesnames in the filesname.txt
      return readFile("filesname.txt");
    })
    .then((filesname) => {
      let names = filesname.trim().split(" ");
      // delete the files simultaneously
      let deletepromises = names.map((filename) => {
        deleteFile(filename);
        console.log(`delete successfully ${filename}`);
      });

      return Promise.all([...deletepromises]);
    })
    .then((data) => {
      console.log("successfully completed...!");
    })
    .catch((error) => {
      console.error(error);
    });
} catch (error) {
  console.error(error);
}
}

main()
